# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class Configuration(metaclass=PoolMeta):
    __name__ = 'party.configuration'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.load_grouping_method.selection.extend([('unit_load', 'Unit load')])


class ConfigurationLoad(metaclass=PoolMeta):
    __name__ = 'party.configuration.load_grouping_method'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.load_grouping_method.selection.extend([('unit_load', 'Unit load')])
