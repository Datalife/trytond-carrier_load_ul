===========================
Carrier load UL many times
===========================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> from trytond.modules.stock_unit_load.tests.tools import create_unit_load
    >>> from proteus import Model, Wizard, Report
    >>> today = datetime.date.today()

Install agro Module::

    >>> config = activate_modules(['carrier_load_ul', 'stock_move_done2cancel'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Create carrier::

    >>> Carrier = Model.get('carrier')
    >>> Uom = Model.get('product.uom')
    >>> Template = Model.get('product.template')
    >>> carrier = Carrier()
    >>> unit, = Uom.find([('name', '=', 'Unit')], limit=1)
    >>> transport_template = Template(
    ...     name='Transport',
    ...     type='service',
    ...     list_price=Decimal(500),
    ...     cost_price=Decimal(0),
    ...     default_uom=unit)
    >>> transport_template.save()
    >>> party_carrier = Party(name='Carrier 1')
    >>> party_carrier.save()
    >>> carrier.party = party_carrier
    >>> carrier.carrier_product = transport_template.products[0]
    >>> carrier.save()
    >>> carrier_product = carrier.carrier_product.template
    >>> carrier_product.purchasable = True
    >>> carrier_product.purchase_uom = carrier_product.default_uom
    >>> carrier_product.account_category = account_category_tax
    >>> carrier_product.save()

Get warehouse and dock::

    >>> Location = Model.get('stock.location')
    >>> wh, = Location.find([('type', '=', 'warehouse')])
    >>> dock = wh.docks.new()
    >>> dock.name = 'Dock 1'
    >>> dock.code = 'D1'
    >>> wh.save()
    >>> wh2, = wh.duplicate()
    >>> wh2.name = 'Warehouse 2'
    >>> wh2.code = 'WH2'
    >>> Dock = Model.get('stock.location.dock')
    >>> d2 = wh2.docks.new()
    >>> d2.name = 'Dock WH2'
    >>> wh2.save()

Create unit load::

    >>> Unitload = Model.get('stock.unit_load')
    >>> ul = create_unit_load(config=config,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> main_product = ul.product
    >>> template = main_product.template
    >>> template.salable = True
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> ul2 = create_unit_load(config=config, product=main_product,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})

Load UL into internal order::

    >>> Load = Model.get('carrier.load')
    >>> Order = Model.get('carrier.load.order')    
    >>> load = Load()
    >>> load.warehouse = wh
    >>> load.carrier = carrier
    >>> load.vehicle_number = 'MX3449'
    >>> load.save()
    >>> load_order = Order(load=load)
    >>> load_order.type = 'internal'
    >>> load_order.start_date = datetime.datetime.now() - relativedelta(minutes=20)
    >>> load_order.to_location = wh2.storage_location
    >>> line = load_order.lines.new()
    >>> line.ul_quantity = Decimal(2)
    >>> load_order.save()
    >>> load_order.click('wait')
    >>> start_load = Wizard('carrier.load_uls', [load_order])
    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    >>> ul.reload()
    >>> ul.load_order == load_order
    True
    >>> ul.load_line == load_order.lines[0]
    True
    >>> start_load.form.ul_code = ul2.code
    >>> start_load.execute('load_')
    >>> start_load.execute('pre_do')
    >>> ul.reload()
    >>> ul.load_order == load_order
    True
    >>> not ul.load_line
    True
    >>> len(ul.load_lines)
    1
    >>> ul.available
    True
    >>> shipment = load_order.shipment
    >>> shipment.click('done')

Create sale and load UL::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.warehouse = wh2
    >>> sale.invoice_method = 'order'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = main_product
    >>> sale_line.quantity = 70.0
    >>> sale_line.ul_quantity = 2.0
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> start_load = Wizard('carrier.load.create_wizard', [sale])
    >>> start_load.form.dock = sale.warehouse.docks[0]
    >>> start_load.form.carrier = carrier
    >>> start_load.form.vehicle_number = 'MX3449'
    >>> start_load.form.trailer_number = 'AC3460'
    >>> start_load.form.driver = 'Driver'
    >>> start_load.form.driver_identifier = 'ID Driver'
    >>> start_load.form.lines[0].ul_quantity = Decimal(1)
    >>> start_load.execute('pre_load_')

    >>> sale.reload()
    >>> load_order2, = sale.loads
    >>> start_load = Wizard('carrier.load_uls', [load_order2])
    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    >>> ul.reload()
    >>> ul.load_order == load_order2 != load_order
    True
    >>> ul.load_line == load_order2.lines[0]
    True
    >>> start_load.execute('pre_do')
    >>> load_order2.reload()
    >>> ul.reload()
    >>> ul.load_order == load_order2
    True
    >>> not ul.load_line
    True
    >>> len(ul.load_lines)
    2

Check restriction unloading::

    >>> start_load = Wizard('carrier.load_uls', [load_order])
    >>> start_load.form.uls_loaded.append(ul)
    >>> start_load.execute('unload_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: You are trying to unload the UL "1 - Product (5.0 Cases, 35.0 u)" from the Load order "1". This is not allowed, probably there are later loads. Please check its load orders. - 