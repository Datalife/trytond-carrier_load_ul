================
Carrier load UL
================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules, set_user
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> from trytond.modules.stock_unit_load.tests.tools import create_unit_load
    >>> from proteus import Model, Wizard, Report
    >>> today = datetime.date.today()

Install agro Module::

    >>> config = activate_modules('carrier_load_ul')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create users::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')

    >>> admin_user, = User.find([('login', '=', 'admin')], limit=1)

    >>> carrier_group, = Group.find([('name', '=', 'Carrier')])
    >>> other_user = User()
    >>> other_user.name = 'Other User'
    >>> other_user.login = 'other_user'
    >>> other_user.groups.append(carrier_group)
    >>> other_user.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Check carrier configuration states::

    >>> Conf = Model.get('carrier.configuration')

    >>> conf = Conf(1)
    >>> conf.shipment_out_state
    'packed'
    >>> conf.sale_state
    'quotation'

Create carrier::

    >>> Carrier = Model.get('carrier')
    >>> Uom = Model.get('product.uom')
    >>> Template = Model.get('product.template')
    >>> carrier = Carrier()
    >>> unit, = Uom.find([('name', '=', 'Unit')], limit=1)
    >>> transport_template = Template(
    ...     name='Transport',
    ...     type='service',
    ...     list_price=Decimal(500),
    ...     cost_price=Decimal(0),
    ...     default_uom=unit)
    >>> transport_template.save()
    >>> party_carrier = Party(name='Carrier 1')
    >>> party_carrier.save()
    >>> carrier.party = party_carrier
    >>> carrier.carrier_product = transport_template.products[0]
    >>> carrier.save()
    >>> carrier_product = carrier.carrier_product.template
    >>> carrier_product.purchasable = True
    >>> carrier_product.purchase_uom = carrier_product.default_uom
    >>> carrier_product.account_category = account_category_tax
    >>> carrier_product.save()

Get warehouse and dock::

    >>> Location = Model.get('stock.location')
    >>> wh, = Location.find([('type', '=', 'warehouse')])
    >>> dock = wh.docks.new()
    >>> dock.name = 'Dock 1'
    >>> dock.code = 'D1'
    >>> wh.save()
    >>> storage_child = Location(name='New storage zone', type='storage')
    >>> storage_child.parent = wh.storage_location
    >>> storage_child.save()

Create carrier load::

    >>> Load = Model.get('carrier.load')
    >>> load = Load()
    >>> load.company != None
    True
    >>> load.state
    'draft'
    >>> load.date == today
    True
    >>> load.warehouse != None
    True
    >>> load.warehouse_output == load.warehouse.output_location
    True
    >>> load.dock != None
    True
    >>> load.carrier = carrier
    >>> load.vehicle_number = 'MX3449'
    >>> load.save()
    >>> load.code != None
    True

Create load order::

    >>> Order = Model.get('carrier.load.order')
    >>> order = Order(load=load)
    >>> order.start_date = datetime.datetime.now() - relativedelta(minutes=20)
    >>> order.state
    'draft'
    >>> bool(order.ul_origin_restrict)
    True
    >>> order.party = customer
    >>> line = order.lines.new()
    >>> line.ul_quantity = Decimal(1)
    >>> order.save()
    >>> order.code != None
    True
    >>> order.date == load.date
    True
    >>> order.click('wait')
    >>> order.state
    'waiting'

Create unit load::

    >>> Unitload = Model.get('stock.unit_load')
    >>> ul = create_unit_load(config=config, do_state=False,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> template = ul.product.template
    >>> template.salable = True
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> main_product = ul.product

Add other products to unit load::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Plastic Case 30x30'
    >>> template.categories.append(category)
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('5')
    >>> product.save()
    >>> move = ul.moves.new()
    >>> move.planned_date = today
    >>> move.product = product
    >>> move.quantity = Decimal(2)
    >>> move.from_location = ul.moves[0].from_location
    >>> move.to_location = ul.moves[0].to_location
    >>> move.currency = move.company.currency
    >>> move.unit_price = product.cost_price
    >>> ul.save()

Starting load wizard::

    >>> start_load = Wizard('carrier.load_uls', [])
    >>> start_load.form.load_order = order
    >>> start_load.execute('post_order')
    >>> start_load.form.load_order == order
    True
    >>> len(start_load.form.uls_loaded)
    0
    >>> start_load.form.loaded_uls
    0
    >>> start_load.form.ul_code = 'X'
    >>> start_load.execute('load_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot find Unit load "X". - 

Check UL loading restrictions::

    >>> bool(Unitload.find([('id', '=', ul.id), ('load_order', '=', None)]))
    True
    >>> bool(Unitload.find([('id', '=', ul.id), ('load_order', '!=', None)]))
    False
    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: UL "1" must be in Done state. - 
    >>> ul.click('assign')
    >>> ul.click('do')
    >>> ul.available
    True
    >>> not Unitload.find([('id', '=', ul.id), ('available', '=', False)])
    True
    >>> start_load.execute('load_')
    >>> bool(Unitload.find([('id', '=', ul.id), ('available', '=', False)]))
    True
    >>> bool(Unitload.find([('id', '=', ul.id), ('load_order', '=', None)]))
    False
    >>> bool(Unitload.find([('id', '=', ul.id), ('load_order', '!=', None)]))
    True
    >>> bool(Unitload.find([('id', '=', ul.id), ('load_order', '=', order)]))
    True
    >>> start_load.form.loaded_uls
    1
    >>> order.reload()
    >>> order.state
    'running'
    >>> order.start_date != None
    True
    >>> len(order.unit_loads)
    1
    >>> order.unit_loads[0].available
    False
    >>> not Unitload.find([('id', '=', order.unit_loads[0].id), ('available', '=', True)])
    True
    >>> bool(Unitload.find([('id', '=', ul.id), ('load_order', '=', None)]))
    False
    >>> bool(Unitload.find([('id', '=', ul.id), ('load_order', '!=', None)]))
    True
    >>> bool(Unitload.find([('id', '=', ul.id), ('load_order', '=', order.id)]))
    True
    >>> bool(Unitload.find([('id', '=', ul.id), ('load_order', '=', order)]))
    True
    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: UL "1" is already loaded. - 


Add an invalid UL::

    >>> Location = Model.get('stock.location')
    >>> lost_found, = Location.find([('type', '=', 'lost_found')], limit=1)
    >>> ul2 = create_unit_load(do_state=False,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> ul2.save()
    >>> ul2.click('do')
    >>> ul3 = create_unit_load(do_state=False,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> ul3.save()
    >>> ul3.click('do')
    >>> move_ul = Wizard('stock.unit_load.do_move', [ul3])
    >>> move_ul.form.location = lost_found
    >>> move_ul.form.planned_date = ul3.end_date + relativedelta(minutes=30)
    >>> move_ul.execute('move_')
    >>> ul3.click('do')
    >>> start_load.form.ul_code = ul3.code
    >>> start_load.execute('load_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: UL "3" must be in a storage location. - 
    >>> start_load.form.ul_code = ul2.code
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='loading_ul_origin_2', always=True).save()
    >>> start_load.execute('load_')
    Traceback (most recent call last):
        ...
    trytond.modules.carrier_load_ul.exceptions.AddUnitLoadOverloadError: All valid lines for UL "2" in load order "1" are complete. - 

Unload UL::

    >>> order.lines[0].ul_quantity += 1
    >>> order.save()
    >>> start_load.execute('load_')
    >>> start_load.form.loaded_uls
    2
    >>> ul2 = Unitload(ul2.id)
    >>> start_load.form.uls_loaded.append(ul2)
    >>> start_load.execute('unload_')
    >>> start_load.form.loaded_uls
    1
    >>> ul2.load_line == None
    True
    >>> start_load.execute('exit')
    >>> order.reload()
    >>> len(order.unit_loads)
    1
    >>> start_load = Wizard('carrier.load_uls', [order])
    >>> start_load.form.ul_code = ul2.code
    >>> start_load.execute('load_')
    >>> start_load.execute('exit')
    >>> order.reload()
    >>> len(order.unit_loads)
    2
    >>> ul2.click('unload')
    >>> order.reload()
    >>> len(order.unit_loads)
    1
    >>> len(order.lines[0].unit_loads)
    1
    >>> len(order.lines[0].loaded_unit_loads)
    0

Finish loading::

    >>> start_load = Wizard('carrier.load_uls', [order])
    >>> start_load.execute('pre_do')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserWarning: You have loaded less ULs (1) than expected (2). - 
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='pending_uls_1', always=True).save()
    >>> start_load.execute('pre_do')
    >>> order.reload()
    >>> len(order.unit_loads)
    1
    >>> order.state
    'done'
    >>> len(order.lines[0].unit_loads)
    0
    >>> len(order.lines[0].loaded_unit_loads)
    1
    >>> order.lines[0].loaded_uls
    1

Check shipment::

    >>> order.reload()
    >>> order.type
    'out'
    >>> shipment = order.shipment
    >>> not shipment
    False
    >>> shipment.state == conf.shipment_out_state
    True
    >>> shipment.state
    'packed'

Check sale::

    >>> not order.sale
    False
    >>> order.sale != None
    True
    >>> order.sale.state == conf.sale_state
    True
    >>> order.sale.state
    'quotation'
    >>> order.sale.number != None
    True
    >>> len(order.sale.lines)
    1
    >>> order.sale.lines[0].product.id == ul.product.id
    True
    >>> len(order.sale.shipments)
    1
    >>> shipment, = order.sale.shipments
    >>> shipment.start_date == order.start_date
    True
    >>> shipment.end_date == order.end_date
    True
    >>> len(shipment.outgoing_moves)
    2
    >>> list(set(m.unit_load.id for m in shipment.outgoing_moves)) == [ul.id]
    True
    >>> len(shipment.inventory_moves)
    2
    >>> list(set(m.unit_load.id for m in shipment.inventory_moves)) == [ul.id]
    True
    >>> len(order.inventory_moves)
    0
    >>> inv_move, = [m for m in shipment.inventory_moves if m.product != ul.product]
    >>> inv_move.product.rec_name
    'Plastic Case 30x30'
    >>> inv_move.quantity
    2.0
    >>> inv_move.start_date == order.start_date
    True
    >>> inv_move.end_date == order.end_date
    True
    >>> len(order.outgoing_moves)
    1
    >>> order.outgoing_moves[0].product.rec_name
    'Plastic Case 30x30'
    >>> order.outgoing_moves[0].quantity
    2.0
    >>> order.outgoing_moves[0].shipment == shipment
    True
    >>> order.shipment == shipment
    True

Force load another UL::

    >>> Unitload = Model.get('stock.unit_load')
    >>> ul = create_unit_load(config=config, do_state=False,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> ul.product = main_product
    >>> ul.save()
    >>> move = ul.moves.new()
    >>> move.planned_date = today
    >>> move.product = product
    >>> move.quantity = Decimal(2)
    >>> move.from_location = ul.moves[0].from_location
    >>> move.to_location = ul.moves[0].to_location
    >>> move.currency = move.company.currency
    >>> move.unit_price = product.cost_price
    >>> ul.save()
    >>> ul.click('assign')
    >>> ul.click('do')
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='loading_ul_origin_3', always=True).save()
    >>> start_load = Wizard('carrier.load_uls', [order])
    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    >>> start_load.execute('exit')
    >>> sale = order.sale
    >>> sale.reload()
    >>> len(sale.lines)
    1
    >>> sale.lines[0].quantity
    70.0
    >>> shipment = sale.shipments[0]
    >>> shipment.reload()
    >>> len(shipment.unit_loads)
    2
    >>> ul in shipment.unit_loads
    True
    >>> len(shipment.outgoing_moves)
    4
    >>> len(shipment.inventory_moves)
    4
    >>> list(set(m.state for m in shipment.outgoing_moves))
    ['assigned']
    >>> list(set(m.state for m in shipment.inventory_moves))
    ['done']
    >>> order.reload()
    >>> len(order.inventory_moves)
    0
    >>> sum(m.quantity for m in shipment.inventory_moves if m.product == product)
    4.0
    >>> len(order.outgoing_moves)
    2
    >>> sum(m.quantity for m in order.outgoing_moves)
    4.0

Add a third UL with done shipment::

    >>> order.lines[0].ul_quantity += 1
    >>> order.save()
    >>> ul = create_unit_load(config=config, do_state=False,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> ul.product = main_product
    >>> ul.save()
    >>> move = ul.moves.new()
    >>> move.planned_date = today
    >>> move.product = product
    >>> move.quantity = Decimal(2)
    >>> move.from_location = ul.moves[0].from_location
    >>> move.to_location = ul.moves[0].to_location
    >>> move.currency = move.company.currency
    >>> move.unit_price = product.cost_price
    >>> ul.save()
    >>> ul.click('assign')
    >>> ul.click('do')
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='loading_ul_origin_4', always=True).save()
    >>> start_load = Wizard('carrier.load_uls', [order])
    >>> shipment.click('done')
    >>> shipment.state
    'done'
    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    >>> shipment.reload()
    >>> len(shipment.unit_loads)
    3
    >>> list(set(m.state for m in shipment.outgoing_moves))
    ['done']
    >>> list(set(m.state for m in shipment.inventory_moves))
    ['done']

Confirm load::

    >>> load.click('confirm')
    >>> load.reload()
    >>> load.state
    'confirmed'

Create purchase::

    >>> bool(load.purchasable)
    False
    >>> load.click('create_purchase')
    >>> load.reload()
    >>> not load.purchase
    True
    >>> load.purchasable = True
    >>> load.save()
    >>> load.currency != None
    True
    >>> load.unit_price = Decimal(300.0)
    >>> load.save()
    >>> not load.purchase_state
    True
    >>> load.click('do')
    >>> load.reload()
    >>> load.state
    'done'
    >>> load.purchase != None
    True
    >>> load.purchase_state
    'quotation'
    >>> order.reload()
    >>> order.carrier_amount
    Decimal('300.00')

Check report carrier load sheet::

    >>> load_sheet = Report('carrier.load.sheet')
    >>> ext, _, _, name = load_sheet.execute([load], {})

Check rerport carrier load purchase::

    >>> wizard = Wizard('carrier.load.print_purchase', [load])
    >>> ext, _, _, name = wizard.actions[0]
    >>> ext
    'odt'
    >>> name
    'Carrier load purchase-1'

Change carrier configuration states::

    >>> Conf = Model.get('carrier.configuration')

    >>> conf = Conf(1)
    >>> conf.shipment_out_state = 'draft'
    >>> conf.sale_state = 'draft'
    >>> conf.save()


Create carrier load::

    >>> load = Load()
    >>> load.carrier = carrier
    >>> load.vehicle_number = 'MX3449'
    >>> load.save()

Create load order::

    >>> order = Order(load=load)
    >>> order.start_date = datetime.datetime.now() - relativedelta(minutes=20)
    >>> order.party = customer
    >>> line = order.lines.new()
    >>> line.ul_quantity = Decimal(1)
    >>> order.click('wait')
    >>> order.state
    'waiting'

Create unit load::

    >>> Unitload = Model.get('stock.unit_load')
    >>> ul = create_unit_load(config=config, do_state=False,
    ...     product=template.products[0],
    ...     default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> ul.click('assign')
    >>> ul.click('do')

Starting load wizard::

    >>> start_load = Wizard('carrier.load_uls', [order])
    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    >>> start_load.execute('pre_do')
    >>> order.reload()
    >>> order.state
    'done'

Check shipment::

    >>> shipment = order.shipment
    >>> not shipment
    False
    >>> shipment.state == conf.shipment_out_state
    True
    >>> shipment.state
    'draft'

Check sale::

    >>> order.sale != None
    True
    >>> order.sale.state == conf.sale_state
    True
    >>> order.sale.state
    'draft'

Create carrier load::

    >>> load = Load()
    >>> load.carrier = carrier
    >>> load.vehicle_number = 'MX3449'
    >>> load.save()

Create load order::

    >>> order = Order(load=load)
    >>> order.start_date = datetime.datetime.now()
    >>> order.party = customer
    >>> line = order.lines.new()
    >>> line.ul_quantity = Decimal(2)
    >>> order.save()
    >>> order.click('wait')
    >>> order.state
    'waiting'

Create unit load::

    >>> uni_load = create_unit_load(config=config,
    ...     product=template.products[0],
    ...     default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})

Starting load wizard::

    >>> start_load = Wizard('carrier.load_uls', [order])
    >>> start_load.form.ul_code = uni_load.code
    >>> start_load.execute('load_')
    >>> order.reload()
    >>> order.ul_quantity > order.loaded_uls
    True

Change User::

    >>> set_user(other_user)

Do load order::

    >>> order.click('do') # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserError: You have loaded less ULs (...) than expected (...). - 
    >>> order.state
    'running'

Change User::

    >>> set_user(admin_user)

Do load order again::

    >>> order.reload()
    >>> order.click('do') # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserWarning: You have loaded less ULs (...) than expected (...). - 
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='pending_uls_%s' % order.id,
    ...     always=True).save()
    >>> order.click('do')
    >>> order.state
    'done'

Create order without ul_quantity::

    >>> order = Order(load=load)
    >>> order.start_date = datetime.datetime.now() - relativedelta(minutes=20)
    >>> order.party = customer
    >>> line = order.lines.new()
    >>> line.ul_quantity == None
    True
    >>> order.save()
    >>> order.ul_quantity == None
    True
    >>> order.click('wait')

Create unit_load::

    >>> ul4 = create_unit_load(config=config, product=main_product,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> ul5 = create_unit_load(config=config, product=main_product,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})

Starting load wizard::

    >>> start_load = Wizard('carrier.load_uls', [order])
    >>> start_load.form.ul_code = ul4.code
    >>> start_load.execute('load_')
    >>> start_load.form.ul_code = ul5.code
    >>> start_load.execute('load_')
    >>> start_load.execute('exit')
    >>> order.reload()
    >>> len(order.unit_loads)
    2