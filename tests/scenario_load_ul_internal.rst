================
Carrier load UL
================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> from trytond.modules.stock_unit_load.tests.tools import create_unit_load
    >>> from proteus import Model, Wizard, Report
    >>> today = datetime.date.today()

Install agro Module::

   >>> config = activate_modules('carrier_load_ul')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Check carrier configuration states::

    >>> Conf = Model.get('carrier.configuration')

    >>> conf = Conf(1)
    >>> conf.shipment_internal_state
    'done'

Create carrier::

    >>> Carrier = Model.get('carrier')
    >>> Uom = Model.get('product.uom')
    >>> Template = Model.get('product.template')
    >>> carrier = Carrier()
    >>> unit, = Uom.find([('name', '=', 'Unit')], limit=1)
    >>> transport_template = Template(
    ...     name='Transport',
    ...     type='service',
    ...     list_price=Decimal(500),
    ...     cost_price=Decimal(0),
    ...     default_uom=unit)
    >>> transport_template.save()
    >>> party_carrier = Party(name='Carrier 1')
    >>> party_carrier.save()
    >>> carrier.party = party_carrier
    >>> carrier.carrier_product = transport_template.products[0]
    >>> carrier.save()
    >>> carrier_product = carrier.carrier_product.template
    >>> carrier_product.purchasable = True
    >>> carrier_product.purchase_uom = carrier_product.default_uom
    >>> carrier_product.save()

Get warehouse and dock::

    >>> Location = Model.get('stock.location')
    >>> wh, = Location.find([('type', '=', 'warehouse')])
    >>> dock = wh.docks.new()
    >>> dock.name = 'Dock 1'
    >>> dock.code = 'D1'
    >>> wh.save()
    >>> wh2, = wh.duplicate()
    >>> wh2.name = 'Warehouse 2'
    >>> wh2.code = 'WH2'
    >>> Dock = Model.get('stock.location.dock')
    >>> d2 = wh2.docks.new()
    >>> d2.name = 'Dock WH2'
    >>> wh2.save()
    >>> storage_child = Location(name='New storage zone', type='storage')
    >>> storage_child.parent = wh.storage_location
    >>> storage_child.save()

Create carrier load::

    >>> Load = Model.get('carrier.load')
    >>> load = Load()
    >>> load.company != None
    True
    >>> load.state
    'draft'
    >>> load.date == today
    True
    >>> load.warehouse = wh
    >>> load.warehouse_output == load.warehouse.output_location
    True
    >>> load.dock != None
    True
    >>> load.carrier = carrier
    >>> load.vehicle_number = 'MX3449'
    >>> load.save()
    >>> load.code != None
    True

Create load order::

    >>> Order = Model.get('carrier.load.order')
    >>> order = Order(load=load)
    >>> order.type = 'internal'
    >>> order.start_date = datetime.datetime.now() - relativedelta(minutes=20)
    >>> order.state
    'draft'
    >>> order.ul_origin_restrict
    True
    >>> order.to_location = wh2.storage_location
    >>> line = order.lines.new()
    >>> line.ul_quantity = Decimal(1)
    >>> order.save()
    >>> order.code != None
    True
    >>> order.date == load.date
    True
    >>> order.click('wait')
    >>> order.state
    'waiting'

Check modify load wizard::

    >>> ModelData = Model.get('ir.model.data')
    >>> action, = ModelData.find([
    ...     ('module', '=', 'carrier_load_ul'),
    ...     ('fs_id', '=', 'wizard_force_load_ul')])
    >>> _ = Wizard('carrier.load_uls', [order], action={'id': action.db_id}) # doctest: +ELLIPSIS
    Traceback (most recent call last):
      ...
    trytond.exceptions.UserError: Can not modify load of the order "..." because is internal type. - 

Create unit load::

    >>> Unitload = Model.get('stock.unit_load')
    >>> ul = create_unit_load(config=config, do_state=False,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> template = ul.product.template
    >>> template.salable = True
    >>> template.save()
    >>> main_product = ul.product

Add other products to unit load::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Plastic Case 30x30'
    >>> template.categories.append(category)
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('5')
    >>> product.save()
    >>> move = ul.moves.new()
    >>> move.planned_date = today
    >>> move.product = product
    >>> move.quantity = Decimal(2)
    >>> move.from_location = ul.moves[0].from_location
    >>> move.to_location = ul.moves[0].to_location
    >>> move.currency = move.company.currency
    >>> move.unit_price = product.cost_price
    >>> ul.save()

Starting load wizard::

    >>> start_load = Wizard('carrier.load_uls', [])
    >>> start_load.form.load_order = order
    >>> start_load.execute('post_order')
    >>> start_load.form.load_order == order
    True
    >>> len(start_load.form.uls_loaded)
    0
    >>> start_load.form.loaded_uls
    0
    >>> start_load.form.ul_code = 'X'
    >>> start_load.execute('load_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot find Unit load "X". - 

Check UL loading restrictions::

    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: UL "1" must be in Done state. - 
    >>> ul.click('assign')
    >>> ul.click('do')
    >>> bool(ul.available)
    True
    >>> not Unitload.find([('id', '=', ul.id), ('available', '=', False)])
    True
    >>> start_load.execute('load_')
    >>> start_load.form.loaded_uls
    1
    >>> order.reload()
    >>> order.state
    'running'
    >>> order.start_date != None
    True
    >>> len(order.unit_loads)
    1
    >>> bool(order.unit_loads[0].available)
    True
    >>> not Unitload.find([('id', '=', order.unit_loads[0].id), ('available', '=', False)])
    True
    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: UL "1" is already loaded. - 


Add an invalid UL::

    >>> Location = Model.get('stock.location')
    >>> lost_found, = Location.find([('type', '=', 'lost_found')], limit=1)
    >>> ul2 = create_unit_load(do_state=False,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> ul2.save()
    >>> ul2.click('do')
    >>> ul3 = create_unit_load(do_state=False,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> ul3.save()
    >>> ul3.click('do')
    >>> move_ul = Wizard('stock.unit_load.do_move', [ul3])
    >>> move_ul.form.location = lost_found
    >>> move_ul.form.planned_date = ul3.end_date + relativedelta(minutes=30)
    >>> move_ul.execute('move_')
    >>> ul3.click('do')
    >>> start_load.form.ul_code = ul3.code
    >>> start_load.execute('load_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: UL "3" must be in a storage location. - 
    >>> start_load.form.ul_code = ul2.code
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='loading_ul_origin_2', always=True).save()
    >>> start_load.execute('load_')
    Traceback (most recent call last):
        ...
    trytond.modules.carrier_load_ul.exceptions.AddUnitLoadOverloadError: All valid lines for UL "2" in load order "1" are complete. - 

Unload UL::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> order.lines[0].ul_quantity += 1
    >>> order.save()
    >>> start_load.execute('load_')
    >>> start_load.form.loaded_uls
    2
    >>> ul2 = UnitLoad(ul2.id)
    >>> start_load.form.uls_loaded.append(ul2)
    >>> start_load.execute('unload_')
    >>> start_load.form.loaded_uls
    1
    >>> ul2.load_line == None
    True
    >>> start_load.execute('exit')
    >>> order.reload()
    >>> len(order.unit_loads)
    1
    >>> start_load = Wizard('carrier.load_uls', [order])
    >>> start_load.form.ul_code = ul2.code
    >>> start_load.execute('load_')
    >>> start_load.execute('exit')
    >>> order.reload()
    >>> len(order.unit_loads)
    2
    >>> ul2.click('unload')
    >>> order.reload()
    >>> len(order.unit_loads)
    1

Finish loading::

    >>> move_ul = Wizard('stock.unit_load.do_move', [ul2])
    >>> move_ul.form.planned_date = datetime.datetime.now() - relativedelta(hours=2)
    >>> move_ul.form.location = storage_child
    >>> move_ul.execute('move_')
    >>> ul2.click('do')
    >>> ul2.location == storage_child
    True
    >>> ul.location != storage_child
    True

    >>> start_load = Wizard('carrier.load_uls', [order])
    >>> start_load.form.ul_code = ul2.code
    >>> start_load.execute('load_')
    >>> ul2.click('unload')

    >>> move_ul = Wizard('stock.unit_load.do_move', [ul])
    >>> move_ul.form.planned_date = datetime.datetime.now() - relativedelta(hours=2)
    >>> move_ul.form.location = storage_child
    >>> move_ul.execute('move_')
    >>> ul.click('do')

    >>> start_load.execute('pre_do')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserWarning: You have loaded less ULs (1) than expected (2). - 
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='pending_uls_1', always=True).save()
    >>> start_load.execute('pre_do')
    >>> order.reload()
    >>> len(order.unit_loads)
    1
    >>> order.state
    'done'
    >>> order.type
    'internal'

Check internal shipment::

    >>> shipment = order.shipment
    >>> not shipment
    False
    >>> shipment.state == conf.shipment_internal_state
    True
    >>> shipment.state
    'done'
    >>> shipment.planned_date == order.end_date.date()
    True
    >>> shipment.effective_date == order.end_date.date()
    True
    >>> shipment.from_location == storage_child.parent
    True
    >>> len(shipment.moves)
    2
    >>> list(set(m.state for m in shipment.moves))
    ['done']
    >>> list(set(m.from_location for m in shipment.moves)) == [storage_child]
    True
    >>> shipment.moves[0].unit_load.id == ul.id
    True
    >>> not order.inventory_moves
    True
    >>> not order.outgoing_moves
    True

Force load another UL::

    >>> ul = create_unit_load(config=config, do_state=False,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> ul.product = main_product
    >>> ul.save()
    >>> move = ul.moves.new()
    >>> move.planned_date = today
    >>> move.product = product
    >>> move.quantity = Decimal(2)
    >>> move.from_location = ul.moves[0].from_location
    >>> move.to_location = ul.moves[0].to_location
    >>> move.currency = move.company.currency
    >>> move.unit_price = product.cost_price
    >>> ul.save()
    >>> ul.click('assign')
    >>> ul.click('do')
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='loading_ul_origin_3', always=True).save()
    >>> start_load = Wizard('carrier.load_uls', [order])
    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    >>> start_load.execute('exit')
    >>> shipment.reload()
    >>> len(shipment.unit_loads)
    2
    >>> ul in shipment.unit_loads
    True
    >>> len(shipment.moves)
    4
    >>> list(set(m.state for m in shipment.moves))
    ['done']
    >>> order.reload()
    >>> not order.inventory_moves
    True
    >>> not order.outgoing_moves
    True

Change carrier configuration states::

    >>> Conf = Model.get('carrier.configuration')

    >>> conf = Conf(1)
    >>> conf.shipment_internal_state = 'draft'
    >>> conf.save()

Create carrier load::

    >>> load = Load()
    >>> load.warehouse = wh
    >>> load.carrier = carrier
    >>> load.vehicle_number = 'MX3449'
    >>> load.save()

Create load order::

    >>> order = Order(load=load)
    >>> order.type = 'internal'
    >>> order.start_date = datetime.datetime.now() - relativedelta(minutes=20)
    >>> order.to_location = wh2.storage_location
    >>> line = order.lines.new()
    >>> line.ul_quantity = Decimal(1)
    >>> order.click('wait')
    >>> order.state
    'waiting'

Create unit load::

    >>> Unitload = Model.get('stock.unit_load')
    >>> ul = create_unit_load(config=config, do_state=False,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> ul.click('assign')
    >>> ul.click('do')

Starting load wizard::

    >>> start_load = Wizard('carrier.load_uls', [order])
    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    >>> start_load.execute('pre_do')
    >>> order.reload()
    >>> order.state
    'done'

Check internal shipment::

    >>> shipment = order.shipment
    >>> not shipment
    False
    >>> shipment.state == conf.shipment_internal_state
    True
    >>> shipment.state
    'draft'
