# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.wizard import (Wizard, StateView, StateAction, StateTransition,
    Button)
from trytond.transaction import Transaction
from trytond.exceptions import UserError, UserWarning
from trytond.i18n import gettext
from .load import CreateLoadDataMixin, CreateLoadDataLineMixin


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    load_lines = fields.Function(
        fields.One2Many('carrier.load.order.line', None, 'Load lines'),
        'get_load_lines')
    ul_quantity_to_load = fields.Function(
        fields.Float('ULs to load', digits=(16, 0),
            help='Available UL quantity to be loaded'),
        'get_ul_quantity_to_load')
    ul_loaded_quantity = fields.Function(
        fields.Float('Loaded ULs', digits=(16, 0)),
        'get_ul_loaded_quantity')

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        for _field_name in ('shipment_method', 'invoice_method'):
            _field = getattr(cls, _field_name, None)
            _field.states['readonly'] |= Eval('loads', [])
            _field.depends.append('load_lines')

    def get_load_lines(self, name=None):
        loads = []
        for line in self.lines:
            if line.load_lines:
                loads.extend(list(map(int, line.load_lines)))
        return loads

    def get_ul_loaded_quantity(self, name=None):
        if self.lines:
            return sum(l.ul_loaded_quantity or 0 for l in self.lines)
        return 0

    def get_ul_quantity_to_load(self, name=None):
        if not self.load_lines:
            return self.ul_quantity
        elif not self.ul_quantity:
            return 0
        return self.ul_quantity - (sum(l.ul_quantity
            for l in self.load_lines) or 0)

    @classmethod
    @ModelView.button_action('carrier_load_ul.wizard_load_sale')
    def load(cls, records):
        pass

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        default = default.copy()
        clear_methods = []
        nonclear_methods = []
        for record in records:
            if record.loads:
                clear_methods.append(record)
            else:
                nonclear_methods.append(record)
        res = super(Sale, cls).copy(nonclear_methods, default=default)
        default.update(cls.default_get(['shipment_method',
            'invoice_method']))
        res.extend(super(Sale, cls).copy(clear_methods, default=default))
        return res

    @property
    def is_origin_load(self):
        LoadOrder = Pool().get('carrier.load.order')
        return bool(self.origin
            and isinstance(self.origin, LoadOrder))

    @classmethod
    def cancel(cls, records):
        for record in records:
            if (record.loads and record.origin
                    and not record.is_origin_load):
                raise UserError(gettext(
                    'carrier_load_ul.msg_sale_sale_cancel_loads',
                    sale=record.rec_name))
        super().cancel(records)

    def check_method(self):
        if not self.loads and not self.is_origin_load:
            super(Sale, self).check_method()
            return
        if (self.shipment_method == 'invoice'
                and self.invoice_method in ('shipment', 'manual')):
            raise UserError(gettext(
                'sale.msg_sale_invalid_method',
                invoice_method=self.invoice_method,
                shipment_method=self.shipment_method,
                sale=self.rec_name))

    @classmethod
    def quote(cls, records):
        cls.restore_load_shipment_method(records)
        super().quote(records)

    @classmethod
    def restore_load_shipment_method(cls, records):
        to_update = []
        for record in records:
            if ((record.shipment_method, record.invoice_method) == (
                    'manual', 'shipment') and not record.loads):
                record.shipment_method = 'order'
                to_update.append(record)
        if to_update:
            cls.save(to_update)

    @classmethod
    def process(cls, sales):
        for sale in sales:
            sale._check_loads()
        super(Sale, cls).process(sales)

    def _check_loads(self):
        pool = Pool()
        Warning = pool.get('res.user.warning')

        if self.state != 'confirmed':
            return
        if self.ul_quantity and not self.loads:
            with Transaction().set_context(_skip_warnings=False):
                warning_name = 'process_noloads_%s' % self.id
                if Warning.check(warning_name):
                    raise UserWarning(warning_name, gettext(
                        'carrier_load_ul.msg_sale_sale_process_noloads',
                        sale=self.rec_name))
        if self.ul_quantity and self.loads and any(
                l.state not in ('cancelled', 'done') for l in self.loads):
            raise UserError(gettext(
                'carrier_load_ul.msg_sale_sale_process_undone_loads',
                sale=self.rec_name))


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    load_lines = fields.One2Many('carrier.load.order.line', 'origin',
        'Load lines', readonly=True)
    ul_loaded_quantity = fields.Function(
        fields.Float('Loaded ULs', digits=(16, 0)),
        'get_ul_loaded_quantity')
    ul_quantity_to_load = fields.Function(
        fields.Float('ULs to load', digits=(16, 0),
            help='Available UL quantity to be loaded'),
        'get_ul_quantity_to_load')

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['load_lines'] = None
        return super(SaleLine, cls).copy(records, default=default)

    def get_ul_loaded_quantity(self, name=None):
        if not self.ul_quantity or not self.load_lines:
            return 0
        return len([ul for l in self.load_lines for ul in l.unit_loads]) or 0

    def get_ul_quantity_to_load(self, name=None):
        if not self.ul_quantity:
            return 0
        if not self.load_lines:
            return self.ul_quantity
        return self.ul_quantity - sum(l.ul_quantity for l in self.load_lines)

    @classmethod
    def delete(cls, records):
        for record in records:
            if any(bool(m.unit_load) for m in record.moves):
                raise UserError(gettext(
                    'carrier_load_ul.msg_sale_line_delete_unit_load',
                    sale_line=record.rec_name))
        super().delete(records)

    def _get_grouping_load_order_key(self):
        return (
            self.sale.company.id,
            self.sale.party.id,
            self.sale.shipment_party.id if self.sale.shipment_party
                else None,
            self.sale.shipment_address.id)


class CreateLoadFromSale(CreateLoadDataMixin, ModelView):
    """Create load from sale"""
    __name__ = 'carrier.load.create_from_sale'

    lines = fields.One2Many('carrier.load.create_from_sale.line',
        None, 'Lines')
    sales = fields.One2Many('sale.sale', None, 'Sales',
        readonly=True)
    load_orders = fields.One2Many('carrier.load.order', None,
        'Order lines', readonly=True)


class CreateLoadLineFromSale(CreateLoadDataLineMixin, ModelView):
    """Create load line from sale"""
    __name__ = 'carrier.load.create_from_sale.line'

    origin = fields.Many2One('sale.line', 'Sale line',
        readonly=True)


class CreateLoad(Wizard):
    """Load Unit load"""
    __name__ = 'carrier.load.create_wizard'

    start = StateTransition()
    sale_data = StateView('carrier.load.create_from_sale',
        'carrier_load_ul.carrier_load_create_from_sale_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('OK', 'pre_load_', 'tryton-ok', default=True)])
    load_ = StateAction('carrier_load.act_load_order')
    pre_load_ = StateTransition()
    post_load_ = StateTransition()

    def transition_start(self):
        pool = Pool()
        Sale = pool.get('sale.sale')

        if not Transaction().context.get('active_ids'):
            return 'end'

        if Transaction().context['active_model'] == 'sale.sale':
            sale = Sale(Transaction().context['active_id'])
            if sale.state not in ('confirmed', 'processing'):
                raise UserError(gettext(
                    'carrier_load_ul.msg_carrier_load_create_wizard_sale_state',
                    sale=sale.rec_name))
            elif sale.state == 'processing' and \
                    sale.shipment_method == 'order':
                raise UserError(gettext(
                    'carrier_load_ul.msg_carrier_load_create_wizard_wrong_shipment_method',
                    sale=sale.rec_name))
            return 'sale_data'
        return 'end'

    def default_sale_data(self, fields):
        pool = Pool()
        Sale = pool.get('sale.sale')
        Conf = pool.get('carrier.configuration')

        conf = Conf(1)
        res = {
            'vehicle_required': conf.vehicle_required,
            'trailer_required': conf.trailer_required,
            'lines': [],
            'origin_name': '%s,%%' % Transaction().context['active_model']
        }
        sale = Sale(Transaction().context.get('active_id'))
        if sale.ul_quantity_to_load <= 0:
            raise UserError(gettext(
                'carrier_load_ul.msg_carrier_load_create_wizard_nothing_load'))
        if not res.get('warehouse', None):
            res.update({'warehouse': sale.warehouse.id})
            if len(sale.warehouse.docks) == 1:
                res.update({'dock': sale.warehouse.docks[0].id})
        for line in sale.lines:
            if line.type != 'line':
                continue
            if line.ul_quantity_to_load <= 0:
                continue
            res['lines'].append(self._get_sale_line_data(line))
        if not res['lines']:
            raise UserError(gettext(
                'carrier_load_ul.msg_carrier_load_create_wizard_nothing_load'))
        res['company'] = sale.company.id
        return res

    def _get_sale_line_data(self, sale_line):
        return {
            'origin': sale_line.id,
            'available_ul_quantity': sale_line.ul_quantity_to_load,
            'ul_quantity': sale_line.ul_quantity_to_load
        }

    @classmethod
    def _get_model_state(cls):
        return {
            'sale.sale': 'sale_data'
        }

    def transition_pre_load_(self):
        pool = Pool()
        Warning = pool.get('res.user.warning')
        LoadOrder = pool.get('carrier.load.order')
        Sale = pool.get('sale.sale')

        _data_state = getattr(self, self._get_model_state()[
            Transaction().context['active_model']])
        if getattr(_data_state, 'load_order', None):
            _load = _data_state.load_order.load
            if _load.state == 'done':
                raise UserError(gettext(
                    'carrier_load_ul.msg_carrier_load_create_wizard_load_done',
                    load=_load.rec_name))
        else:
            _load = self._get_load()
            if not _load.carrier and not _load.carrier_info:
                raise UserError(gettext(
                    'carrier_load.msg_carrier_load_missing_carrier_info',
                    load=_load.rec_name))

        orders = []
        load_order_keys = {}

        load_order2uls = {}
        for line in _data_state.lines:
            if line.ul_quantity <= 0:
                continue
            _key = line.origin._get_grouping_load_order_key()
            load_order_keys.setdefault(_key, None)
            load_order = self._get_load_order(load_order_keys, line)
            if not getattr(_data_state, 'load_order', None):
                load_order.load = _load
            load_order.save()
            _load_line = self._get_load_order_line(load_order, line)
            _load_line.order = load_order
            _load_line.save()
            if getattr(_data_state, 'unit_loads', []):
                uls = self._get_uls_to_load(line, _data_state.unit_loads)
                if uls:
                    if load_order.state in ('draft', 'waiting'):
                        LoadOrder.run([load_order])
                    load_order2uls.setdefault(load_order, []).extend(uls)
            orders.append(load_order)
        for load_order, uls in load_order2uls.items():
            load_order.add_ul(uls)
        if len(load_order_keys) > 1:
            warning_name = 'many_orders_%s' % Transaction().user
            if Warning.check(warning_name):
                raise UserWarning(warning_name, gettext(
                    'carrier_load_ul.msg_carrier_load_create_wizard_many_orders',
                    keys=len(load_order_keys)))

        # update sale
        if Transaction().context['active_model'] == 'sale.sale':
            sale = Sale(Transaction().context['active_id'])
            sale.shipment_method = 'manual'
            sale.invoice_method = 'shipment'
            sale.save()
        _data_state.load_orders = orders
        return 'load_'

    def _get_load_orders(self):
        _data_state = getattr(self, self._get_model_state()[
            Transaction().context['active_model']])
        return list(set([o for o in _data_state.load_orders]))

    def do_load_(self, action):
        order_ids = [o.id for o in self._get_load_orders()]
        data = {'res_id': order_ids}
        if len(order_ids) == 1:
            action['views'].reverse()
        return action, data

    def transition_load_(self):
        return 'post_load_'

    def transition_post_load_(self):
        return 'end'

    def _get_uls_to_load(self, line, uls):
        return [ul for ul in uls
            if ul.sale_line.id == line.origin.id]

    def _get_load_record(self, Load):
        return Load()

    def _get_load(self):
        pool = Pool()
        Load = pool.get('carrier.load')

        _data_state = getattr(self, self._get_model_state()[
            Transaction().context['active_model']])
        _load = self._get_load_record(Load)
        _load.carrier = _data_state.carrier
        _load.carrier_info = _data_state.carrier_info
        _load.vehicle_number = _data_state.vehicle_number
        _load.trailer_number = _data_state.trailer_number
        _load.warehouse = _data_state.warehouse
        _load.dock = _data_state.dock
        _load.purchasable = _data_state.load_purchasable
        _load.driver = _data_state.driver
        _load.driver_identifier = _data_state.driver_identifier
        _load.save()
        return _load

    def _get_load_order(self, load_order_keys, line):
        pool = Pool()
        Order = pool.get('carrier.load.order')

        _data_state = getattr(self, self._get_model_state()[
            Transaction().context['active_model']])
        if getattr(_data_state, 'load_order', None):
            if len(load_order_keys) > 1:
                raise UserError(gettext(
                    'carrier_load_ul.'
                    'msg_carrier_load_create_wizard_wrong_load_order',
                    order=self.data.load_order.rec_name))
            _key = next(iter(load_order_keys.keys()))
            if (_data_state.load_order.lines
                and any(_key != l.origin._get_grouping_load_order_key()
                    for l in _data_state.load_order.lines)):
                raise UserError(gettext(
                    'carrier_load_ul.'
                    'msg_carrier_load_create_wizard_wrong_load_order',
                    order=_data_state.load_order.rec_name))
            return _data_state.load_order

        key = line.origin._get_grouping_load_order_key()
        if not load_order_keys.get(key, None):
            order = Order(
                company=key[0],
                type='out',
                lines=[])
            if Transaction().context['active_model'] == 'sale.sale':
                order.party = line.origin.sale.shipment_party or \
                    line.origin.sale.party
                order.sale = Transaction().context['active_id']
            load_order_keys[key] = order

        return load_order_keys.get(key)

    def _get_load_order_line(self, load_order, line):
        pool = Pool()
        Line = pool.get('carrier.load.order.line')

        for load_line in load_order.lines:
            if load_line.origin == line.origin:
                load_line.ul_quantity += line.ul_quantity
                return load_line
        return Line(
            origin=line.origin,
            ul_quantity=line.ul_quantity)
