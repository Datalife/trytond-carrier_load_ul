# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class Template(metaclass=PoolMeta):
    __name__ = 'carrier.load.cmr.template'

    @classmethod
    def _get_section_defaults(cls):
        defaults = super()._get_section_defaults()
        defaults['6-9'] = \
            "TOTAL PALLETS: ${lang.format('%d', record.loaded_uls)}"
        return defaults
